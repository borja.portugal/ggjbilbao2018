// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GGJBilbao2018GameMode.generated.h"

UCLASS(MinimalAPI)
class AGGJBilbao2018GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGGJBilbao2018GameMode();
};



