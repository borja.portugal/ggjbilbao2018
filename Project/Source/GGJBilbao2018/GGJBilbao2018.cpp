// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GGJBilbao2018.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, GGJBilbao2018, "GGJBilbao2018");

DEFINE_LOG_CATEGORY(LogFlying)
