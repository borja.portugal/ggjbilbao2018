# Firefly Effect

**Gameplay Video:** https://youtu.be/uGLbDI5azYM

**Download:** https://globalgamejam.org/2018/games/paperfly-effect

## High concept
Firefly effect is a 3D playing sandbox where the player encarnates a butterfly and has to break havoc. 

## How to play
The player movement is similar to the one used for helicopters, the movement inputs will change the rientation and the Flap will perform the actual movement.

 - Keyboard
  - WASD: Move player
  - SPACE: Flap
  - M: Wind attack
  - SHIFT: Stabilize
		
 - Controller
  - LEFT TRIGGER: Move player
  - A: Flap
  - RIGHT TRIGGER: Wind attack 
  - LEFT TRIGGER: Stabilize


## Credits
 - Programming
  - Borja Portugal
  - Gorka Martin
  - Jon Diego
  - Laura Barrenengoa
	
 - Art
  - Arantzazu Martinez
  - Eurie Cierbide
  - Leire Acha

  - Sound:
   - Ioba Lopez


## Special thanks
 - Global Game Jam Bilbao staff
 - Azkuna Zentroa





